﻿using System;

namespace udc.pim.model
{
    public class PimCallInfo
    {
        public DateTime CallTime { get; set; }
        public string FromName { get; set; }
        public string FromNumber { get; set; }
        public string ToNumber { get; set; }        
    }
}
