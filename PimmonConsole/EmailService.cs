﻿using MimeKit;
using MailKit.Net.Smtp;
using System.Threading.Tasks;

namespace PimmonConsole
{
    public class EmailService
    {
        public void SendEmail(string emailFrom,
            string emailFromName,
            string emailTo, 
            string subject, 
            string message, 
            string smtpServer,
            int smtpPort)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress(emailFromName, emailFrom));
            emailMessage.To.Add(new MailboxAddress(emailTo));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message
            };

            using (var client = new SmtpClient())
            {
                client.Connect(smtpServer, smtpPort, false);
                client.Send(emailMessage);
                client.Disconnect(true);
            }
        }
    }
}
