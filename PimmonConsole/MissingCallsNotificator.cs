﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using udc.pim.model;

namespace PimmonConsole
{
    public class MissingCallsNotificator
    {

        EmailService _emailService;
        private string emailTo;
        private string subject;
        private string emailFrom;
        private string emailFromName;
        private string smtpServer;
        private int smtpPort;

        public MissingCallsNotificator(IConfigurationRoot config)
        {            
            emailTo = config["emailTo"];
            subject = config["emailSubject"];
            emailFrom = config["emailFrom"];
            emailFromName = config["emailFromName"];
            smtpServer = config["smtpServer"];
            smtpPort = int.Parse(config["smtpPort"]);
            _emailService = new EmailService();
        }
        public void SendNotification(List<PimCallInfo> missedCalls)
        {
            if (missedCalls.Count == 0)
            {
                Console.WriteLine("Pimphony: no new missed calls.");
                return;
            }
            string htmlMessage = FormatEmail(missedCalls);
            _emailService.SendEmail(emailFrom, 
                emailFromName, 
                emailTo, 
                subject, 
                htmlMessage,
                smtpServer,
                smtpPort);
        }

        string FormatEmail(List<PimCallInfo> missedCalls)
        {            
            StringBuilder htmlMessage = new StringBuilder();
            string cssTableStyle = "<style>table, th, td {border: 1px solid black; border-collapse: collapse; padding: 10px;}</style>";
            string htmlTableHeader = "<div>" +
                "<table>" +
                "<colgroup><col><col><col><col></colgroup>" +
                "<tbody><tr><th>Time</th><th>Name</th><th>From</th><th>To</th></tr>";
            htmlMessage.Append("Total " + missedCalls.Count.ToString() + " new missed calls:");
            htmlMessage.Append(cssTableStyle);
            htmlMessage.Append(htmlTableHeader);
            foreach (var call in missedCalls.OrderBy(o => o.CallTime).ToList())
            {
                htmlMessage.Append("<tr><td>" + call.CallTime + "</td><td>" + call.FromName + "</td><td>" + call.FromNumber + "</td><td>" + call.ToNumber + "</td></tr>");
            }
            htmlMessage.Append("</tbody></table></div>");
            return htmlMessage.ToString();
        }
    }
}
