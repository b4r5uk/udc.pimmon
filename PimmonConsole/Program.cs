﻿using System;
using udc.pim.db;
using Microsoft.Extensions.Configuration;

namespace PimmonConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", true, true)
                .Build();
            var db = new PimDbContext();            
            var missedCalls = db.GetMissedCalls();
            if (missedCalls.Count > 0)
            {
                Console.WriteLine("Pimphony: " + missedCalls.Count.ToString() + " missed calls.");
                var notificator = new MissingCallsNotificator(config);
                notificator.SendNotification(missedCalls);
            }
            else
            {
                Console.WriteLine("Pimphony: no new missed calls.");
            }
            //
            //
        }
    }
}
