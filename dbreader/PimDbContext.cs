﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Globalization;
using System.IO;
using System.Linq;
using udc.pim.model;
using System.Runtime.Serialization.Formatters.Binary;
using System.Reflection;

namespace udc.pim.db
{
    public class PimDbContext
    {
        public PimDbContext(string pimDbPath = null)
        {
            if (pimDbPath == null)
            {
                pimDbPath = DefaultPimDbPath;
            }

            _calls = ReadMissedCallsFromPimDb(pimDbPath);
        }

        string lastCheckFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "LastCheck.datetime");
        DateTime _lastCheck;
        public DateTime LastCheck
        {
            get
            {
                if (File.Exists(lastCheckFile))
                {
                    using (Stream openFileStream = File.OpenRead(lastCheckFile))
                    {
                        BinaryFormatter deserializer = new BinaryFormatter();
                        _lastCheck = (DateTime)deserializer.Deserialize(openFileStream);
                        return _lastCheck;
                    }                    
                }
                else
                {
                    using (Stream createFileStream = File.Create(lastCheckFile))
                    {
                        BinaryFormatter serializer = new BinaryFormatter();
                        serializer.Serialize(createFileStream, DateTime.MinValue);
                    }
                    return DateTime.MinValue;
                }                
            }
            private set
            {
                using (Stream saveFileStream = File.Create(lastCheckFile))
                {
                    BinaryFormatter serializer = new BinaryFormatter();
                    serializer.Serialize(saveFileStream, value);
                }
            }
        }


        string DefaultPimDbPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Alcatel PIMphony\\log.mdb");
        List<PimCallInfo> _calls;
        

        List<PimCallInfo> ReadMissedCallsFromPimDb(string MdbPath)
        {
            List<PimCallInfo> allMissedCallsList = new List<PimCallInfo>();
            string connString = "Driver={Microsoft Access Driver (*.mdb, *.accdb)}; Dbq=" + MdbPath + ";Uid=Admin;Pwd=;";
            string queryString = "select * from Logs where (InOut = 2 OR InOut = 4) AND State = 65";

            using (OdbcConnection conn = new OdbcConnection(connString))
            {
                OdbcCommand cmd = new OdbcCommand(queryString, conn);
                try
                {
                    conn.Open();
                    OdbcDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        allMissedCallsList.Add(new PimCallInfo
                        {
                            CallTime = ConcatDateAndTime(reader.GetDateTime(1), reader.GetDateTime(2)),
                            FromNumber = reader.GetString(5),
                            ToNumber = reader.GetString(6),
                            FromName = reader.GetString(4)
                        });

                    }
                    reader.Close();
                    conn.Close();
                    DateTime oldLastCheck = LastCheck;
                    LastCheck = DateTime.Now;
                    return allMissedCallsList.Where(x => x.CallTime > oldLastCheck).ToList();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return null;
                }
            }
        }

        DateTime ConcatDateAndTime(DateTime date, DateTime time)
        {
            return date.Date.Add(time.TimeOfDay);
        }

        public string GetMissedCallsString()
        {
            string callsList = "";
            foreach (var call in _calls)
            {
                string record = call.CallTime.ToString() + " :: " + call.FromName + " :: " + call.FromNumber + " :: " + call.ToNumber;
                callsList += record + "\n";
            }

            return callsList;
        }

        public List<PimCallInfo> GetMissedCalls()
        {
            return _calls;
        }
    }
}
